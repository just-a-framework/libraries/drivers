#pragma once

#include <cstdint>

#include <boards/digital_pin.h>
#include <boards/analog_pin.h>

namespace drivers::l298n
{
	enum class side
	{
		left,
		right
	};
	
	enum class direction
	{
		forward,
		backward
	};
	
	struct simple_controller
	{
		simple_controller(boards::digital_pin& left_forward_pin, 
					boards::digital_pin& left_backward_pin, 
					boards::digital_pin& right_forward_pin, 
					boards::digital_pin& right_backward_pin)
			: left_forward_pin_{ left_forward_pin }
			, left_backward_pin_{ left_backward_pin }
			, right_forward_pin_{ right_forward_pin }
			, right_backward_pin_{ right_backward_pin }
		{
			using namespace boards;
			
			left_forward_pin_.mode(pin_mode::output);
			left_backward_pin_.mode(pin_mode::output);
			right_forward_pin_.mode(pin_mode::output);
			right_backward_pin_.mode(pin_mode::output);
		}
		
		void stop()
		{
			using namespace boards;
			
			left_forward_pin_.state(pin_state::low);
			left_backward_pin_.state(pin_state::low);
			right_forward_pin_.state(pin_state::low);
			right_backward_pin_.state(pin_state::low);
		}
		
		void move(direction dir, side s)
		{
			using namespace boards;
			
			switch (s)
			{
			case side::left:
				switch (dir)
				{
					case direction::forward:
						left_forward_pin_.state(pin_state::high);
						left_backward_pin_.state(pin_state::low);
					break;
					case direction::backward:
						left_forward_pin_.state(pin_state::low);
						left_backward_pin_.state(pin_state::high);
					break;
				}
			break;
			case side::right:
				switch (dir)
				{
					case direction::forward:
						right_forward_pin_.state(pin_state::high);
						right_backward_pin_.state(pin_state::low);
					break;
					case direction::backward:
						right_forward_pin_.state(pin_state::low);
						right_backward_pin_.state(pin_state::high);
					break;
				}
			break;
			}
		}
		
	private:
		boards::digital_pin& left_forward_pin_;
		boards::digital_pin& left_backward_pin_;
		boards::digital_pin& right_forward_pin_;
		boards::digital_pin& right_backward_pin_;
	};
	
	enum class speed
	{
		slow,
		normal,
		fast,
		max
	};
	
	template <typename value_type>
	class speed_controller : simple_controller
	{
	public:
		
		speed_controller(boards::digital_pin& left_forward_pin, 
					boards::digital_pin& left_backward_pin, 
					boards::analog_pin<value_type>& left_speed_pin, 
					boards::digital_pin& right_forward_pin, 
					boards::digital_pin& right_backward_pin,
					boards::analog_pin<value_type>& right_speed_pin)
			: simple_controller{ left_forward_pin, left_backward_pin, right_forward_pin, right_backward_pin }
			, left_speed_pin_{ left_speed_pin }
			, right_speed_pin_{ right_speed_pin }
		{
		}
		
		void move(direction dir, side s, speed move_speed)
		{
			using namespace boards;
			
			switch (s)
			{
			case side::left:
				left_speed_pin_.value(convert_speed(move_speed));
			break;
			case side::right:
				right_speed_pin_.value(convert_speed(move_speed));
			break;
			}
			move(dir, s);
		}
		
	private:
		boards::analog_pin<value_type>& left_speed_pin_;
		boards::analog_pin<value_type>& right_speed_pin_;
		
		value_type convert_speed(speed s)
		{
			switch (s)
			{
			case speed::slow:
				return 150 * sizeof(value_type);
			case speed::normal:
				return 180 * sizeof(value_type);
			case speed::fast:
				return 220 * sizeof(value_type);
			case speed::max:
				return 255 * sizeof(value_type);
			}
			return 0;
		}
	};
}
