#pragma once

#include <cstdint>
#include <array>
#include <util/delay.h>

#include <boards/digital_pin.h>

namespace drivers::sn74hc595
{
	template <uint32_t count>
	struct chain
	{
		chain(boards::digital_pin& data_pin, 
				boards::digital_pin& shift_pin, 
				boards::digital_pin& store_pin)
			: data_pin_{ data_pin }
			, shift_pin_{ shift_pin }
			, store_pin_{ store_pin }
		{
			using namespace boards;
			
			data_pin_.mode(pin_mode::output);
			shift_pin_.mode(pin_mode::output);
			store_pin_.mode(pin_mode::output);
		}
		
		void write(std::array<boards::pin_state, count * 8> data)
		{
			using namespace boards;
			
			store_pin_.state(pin_state::low);
			for(auto e : data)
			{
				shift_pin_.state(pin_state::low);
				data_pin_.state(e);
				shift_pin_.state(pin_state::high);
			}
			store_pin_.state(pin_state::high);
		}
		
	private:
		boards::digital_pin& data_pin_;
		boards::digital_pin& shift_pin_;
		boards::digital_pin& store_pin_;
	};
	
	struct simple : chain<1>
	{
		simple(boards::digital_pin& data_pin, 
				boards::digital_pin& shift_pin, 
				boards::digital_pin& store_pin)
			: chain<1>{ data_pin , shift_pin , store_pin }
		{
		}
	};
}
